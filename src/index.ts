import { start } from "repl";

// import { configT,STATES_T,STATE_RESULT_T,wrapperBody,wrapperBodyKeys,StringifiedMessageI,KafkaMessage } from "./types";

// color codes for console
// https://gist.github.com/abritinthebay/d80eb99b2726c83feb0d97eab95206c4
const EventEmitter = require('events');
// ! disconnect kafka or rabit mq on process exit
const kafka = require('kafka-node');
const amqp = require('amqplib');

export type STATES_T = 'create-cms' |
  'create-cms-rollback' |
  'create-url' |
  'create-url-rollback' |
  'create-tag' |
  'create-tag-rollback' |
  'put-wms-product' |
  'put-wms-product-rollback' |
  'patch-asset-manager' |
  'patch-asset-manager-rollback' |

  // concatinated states
  'create-cms:done' |
  'create-cms:failed' |
  'create-cms-rollback:done' |
  'create-cms-rollback:failed' |
  'create-url:done' |
  'create-url:failed' |
  'create-url-rollback:done' |
  'create-url-rollback:failed' |
  'create-tag:done' |
  'create-tag:failed' |
  'create-tag-rollback:done' |
  'create-tag-rollback:failed' |
  'put-wms-product:done' |
  'put-wms-product:failed' |
  'put-wms-product-rollback:done' |
  'put-wms-product-rollback:failed' |
  'patch-asset-manager:done' |
  'patch-asset-manager:failed' |
  'patch-asset-manager-rollback:done' |
  'patch-asset-manager-rollback:failed'

export type STATE_RESULT_T = 'done' | 'failed';


export type settings = {
  [K in STATE_RESULT_T]?: {
    nextState?: {
      topic: string,
      state: STATES_T,
      keyInRequestBody: wrapperBodyKeys,
      mandatoryKey?: boolean  // default nature is to all are optional, if we specify it to be true, it is required
    },
    rollback: boolean,
    success: boolean,
  }
}

export type SEQUENCE_OF_STATES_T = { [K in STATES_T]?: settings }

export interface configT {
  QueConfig: {
    url?: string,
    consumerChannel: string
  },
  SEQUENCE_OF_STATES: SEQUENCE_OF_STATES_T
}


export type wrapperBodyKeys = 'cms' | 'cmsRollback' | 'product' | 'productRollback' | 'productMeta' | 'productMetaRollback' | 'urlManager' |
  'urlManagerRollback' | 'tag' | 'tagRollback' | 'wms' | 'wmsRollback' | 'assetManager' | 'assetManagerRollback' | 'lookbook' | 'lookbookRollback' | 'lookbookCategory' | 'lookbookCategoryRollback' | 
  'collection' | 'collectionRollback' | 'pages' | 'pagesRollback' | 'pagesCategory' | 'pagesCategoryRollback'

export type wrapperBodyWithoutCommonProperties = {
  [k in wrapperBodyKeys]?: any
}

export type commonProperties = { commonProperties?: { vendorCode: string, source: string, sourceId: string } }

export type wrapperBody = wrapperBodyWithoutCommonProperties & commonProperties;
export interface StringifiedMessageI {
  reqID: string,
  message: wrapperBody,
  state: STATES_T,
  error: string,
  replyTopic: string

}
interface KafkaMessage {
  value: string,
  topic: string,
  offset: number,
  partition: number,
  highWaterOffset: number,
  key?: string
}

type QUE_TYPE = 'kafka' | 'rabitMQ';


let CONSUMER: any;

let producer: any;

class Common {
  public consumerEventEmitter: any;
  public rabbitMqChannel: any;
  public producer: any;

  constructor(public config: configT, public QUE: QUE_TYPE) {
    this.consumerEventEmitter = new EventEmitter()
  }

  processMessage(parsedMessage: StringifiedMessageI): any {

    if (parsedMessage.state.split(':').length !== 2) {
      console.log(`state should have ':', since we are expecting replay on action`);
      return;
    }

    const STATE = <STATES_T>parsedMessage.state.split(':')[0];
    const STATE_RESULT = <STATE_RESULT_T>parsedMessage.state.split(':')[1]

    // validation of state
    if (this.config.SEQUENCE_OF_STATES[STATE] === undefined || this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT] === undefined) {
      console.log(`${STATE} is not valid state or ${STATE_RESULT} is not valid STATE_RESULT`);
      return;
    }

    // !check if its a rollback state and need to update
    if (this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.rollback) {
      this.consumerEventEmitter.emit(`ROLLBACK`, parsedMessage)
    }

    //* if success and we need to update
    if (this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.success) {
      this.consumerEventEmitter.emit(`SUCCESS`, parsedMessage)
    }

    //?  if next state is present
    if (this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState !== undefined) {

      // if next state is to rollback , check rollback object in messageValue
      if (this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state.indexOf(`:rollback`) > -1) {
        let targetRollbackObject = parsedMessage.message[<wrapperBodyKeys>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.keyInRequestBody!}Rollback`]
        //  will definately return from here if rollback object not found
        if (targetRollbackObject === undefined) {
          if (this.config.SEQUENCE_OF_STATES[<STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:done`] !== undefined) {
            this.processMessage({ ...parsedMessage, state: <STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:done` })
          }
          return;
        }

      }


      //  we can skip to next state if keyInRequestBody is not present in message(request body) and current-state:done is valid state

      let targetObject = parsedMessage.message[<wrapperBodyKeys>this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.keyInRequestBody!]

      //  we are not send next message if next object is undefined, hence we have return statement in this if
      if (targetObject === undefined) {

        // start rollback if this state is required
        if (this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.mandatoryKey) {
          if (this.config.SEQUENCE_OF_STATES[<STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:failed`] !== undefined) {
            return this.processMessage({
              ...parsedMessage,
              state: <STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:failed`,
              error: `${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.keyInRequestBody!} is required`
            })
          }
        }


        if (this.config.SEQUENCE_OF_STATES[<STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:done`] !== undefined) {
          return this.processMessage({ ...parsedMessage, state: <STATES_T>`${this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state}:done` })
        }

      }

      this.produceMessage(this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.topic, this.config.SEQUENCE_OF_STATES[STATE]![STATE_RESULT]!.nextState!.state, parsedMessage.reqID, parsedMessage.message, this.config.QueConfig.consumerChannel);

    }

  }

  produceMessage(topic: string, state: STATES_T, reqID: string, message: any, replyTopic: string, error?: string) {
    return new Promise((resolve, reject) => {
      let messageString = JSON.stringify({ reqID, message, replyTopic, state, error })
      switch (this.QUE) {
        case `kafka`:
          if (this.producer === undefined) {
            return reject(new Error(`producer not ready`))
          }

          this.producer.send([
            {
              topic: topic,
              messages: messageString
            }
          ], (err: any, data: any) => {
            if (err) {
              // console.log(err);
              return reject(err)
            } else {
              // console.log(`${reqID}: current state: ${state}`);
              console.log('\x1b[33m%s\x1b[0m', `message sent to topic ${topic}, state: ${state}`);

              return resolve()
            }
          });
          break;
        case `rabitMQ`:
          if (this.rabbitMqChannel === undefined) {
            return reject(new Error(`channel not ready`))
          }
          this.rabbitMqChannel.sendToQueue(topic, Buffer.from(messageString), {
            persistent: true
          });
          console.log('\x1b[33m%s\x1b[0m', `message sent to topic ${topic}, state: ${state}`);

          return resolve()

          break;

        default:
          break;
      }

    })
  }

}

export class RabitMq extends Common {

  constructor(configi: configT, public startConsuming?: boolean) {
    super({ QueConfig: { url: `amqp://localhost` }, ...configi }, `rabitMQ`)
  }

  // init method to inilize async connection
  async init() {
    let connection = await amqp.connect(this.config.QueConfig.url)
    this.rabbitMqChannel = await connection.createChannel()
    this.rabbitMqChannel.assertQueue(this.config.QueConfig.consumerChannel, {
      durable: true
    });

    this.startConsumingMessagesRabitMq();

  }

  startConsumingMessagesRabitMq() {
    //  todo: type for message from rabitMQ
    this.rabbitMqChannel.consume(this.config.QueConfig.consumerChannel, (msg: any) => {
      let parsedMessage: StringifiedMessageI = JSON.parse(msg.content.toString());
      console.log('\x1b[32m', `message received, state: ${parsedMessage.state}`);

      if (!this.startConsuming) {
        this.consumerEventEmitter.emit('message', parsedMessage)
      } else {
        this.processMessage(parsedMessage)
      }
    }, {
        noAck: true
      })
  }
}
export class Kafka extends Common {

  constructor(configi: configT, public startConsuming?: boolean) {
    super({ QueConfig: { url: `localhost:2181` }, ...configi }, `kafka`)
  }

  // init method to inilize async connection
  async init() {
    let topicsToCreate = [
      {
        topic: this.config.QueConfig.consumerChannel,
        partitions: 1,
        replicationFactor: 1
      }
    ];

    let client = await this.connectClientAndCreateTopics(this.config.QueConfig.url!, topicsToCreate);
    let CONSUMER = await this.connectConsumer(client, this.config.QueConfig.consumerChannel);

    this.producer = await this.connectProducer(client);

    this.startConsumingMessages(CONSUMER, this.startConsuming);

  }

  connectClientAndCreateTopics(url: string, topicsToCreate: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let client = new kafka.KafkaClient(url);

      client.on(`ready`, () => {


        client.createTopics(topicsToCreate, (error: any, result: any) => {
          if (error) {
            console.log(`error creating topics`);
            return reject(error)
          }

          return resolve(client);
        })

      })

      client.on(`error`, (error: any) => {
        console.log(`client on error`);
        return reject(error);
      })

    })
  }
  connectConsumer(client: any, topic: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let Consumer = kafka.Consumer;
      let consumer = new Consumer(
        client,
        [{ topic: topic }],
        // [{ topic: kafkaConfig.consumerChannel }],
        {
          autoCommit: true,
          fetchMaxWaitMs: 1000,
          fetchMaxBytes: 1024 * 1024,
          encoding: 'utf8',
          fromOffset: false
        }
      );
      return resolve(consumer);

    })
  }
  connectProducer(client: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let Producer = kafka.Producer;
      let producer = new Producer(client, { partitionerType: 2 });
      return resolve(producer);
    })
  }

  startConsumingMessages(consumer: any, orchastrator?: boolean) {
    consumer.on('message', async (message: KafkaMessage) => {

      let parsedMessage: StringifiedMessageI = JSON.parse(message.value);
      // console.log(parsedMessage.message);

      console.log('\x1b[32m', `message received, state: ${parsedMessage.state}`);
      // validation of state
      if (!orchastrator) {
        this.consumerEventEmitter.emit('message', parsedMessage)
      } else {
        this.processMessage(parsedMessage)
      }
    })
  }
}

export async function rabitMqObject(configi: configT, startConsuming?: boolean) {
  let rabitMqObject = new RabitMq(configi, startConsuming);
  await rabitMqObject.init();
  return rabitMqObject;
}

export async function kafkaObject(configi: configT, startConsuming?: boolean) {
  let kafkaObject = new Kafka(configi, startConsuming);
  await kafkaObject.init();
  return kafkaObject;
}


