"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { configT,STATES_T,STATE_RESULT_T,wrapperBody,wrapperBodyKeys,StringifiedMessageI,KafkaMessage } from "./types";
// color codes for console
// https://gist.github.com/abritinthebay/d80eb99b2726c83feb0d97eab95206c4
var EventEmitter = require('events');
// ! disconnect kafka or rabit mq on process exit
var kafka = require('kafka-node');
var amqp = require('amqplib');
var CONSUMER;
var producer;
var Common = /** @class */ (function () {
    function Common(config, QUE) {
        this.config = config;
        this.QUE = QUE;
        this.consumerEventEmitter = new EventEmitter();
    }
    Common.prototype.processMessage = function (parsedMessage) {
        if (parsedMessage.state.split(':').length !== 2) {
            console.log("state should have ':', since we are expecting replay on action");
            return;
        }
        var STATE = parsedMessage.state.split(':')[0];
        var STATE_RESULT = parsedMessage.state.split(':')[1];
        // validation of state
        if (this.config.SEQUENCE_OF_STATES[STATE] === undefined || this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT] === undefined) {
            console.log(STATE + " is not valid state or " + STATE_RESULT + " is not valid STATE_RESULT");
            return;
        }
        // !check if its a rollback state and need to update
        if (this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].rollback) {
            this.consumerEventEmitter.emit("ROLLBACK", parsedMessage);
        }
        //* if success and we need to update
        if (this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].success) {
            this.consumerEventEmitter.emit("SUCCESS", parsedMessage);
        }
        //?  if next state is present
        if (this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState !== undefined) {
            // if next state is to rollback , check rollback object in messageValue
            if (this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state.indexOf(":rollback") > -1) {
                var targetRollbackObject = parsedMessage.message[this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.keyInRequestBody + "Rollback"];
                //  will definately return from here if rollback object not found
                if (targetRollbackObject === undefined) {
                    if (this.config.SEQUENCE_OF_STATES[this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":done"] !== undefined) {
                        this.processMessage(__assign({}, parsedMessage, { state: this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":done" }));
                    }
                    return;
                }
            }
            //  we can skip to next state if keyInRequestBody is not present in message(request body) and current-state:done is valid state
            var targetObject = parsedMessage.message[this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.keyInRequestBody];
            //  we are not send next message if next object is undefined, hence we have return statement in this if
            if (targetObject === undefined) {
                // start rollback if this state is required
                if (this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.mandatoryKey) {
                    if (this.config.SEQUENCE_OF_STATES[this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":failed"] !== undefined) {
                        return this.processMessage(__assign({}, parsedMessage, { state: this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":failed", error: this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.keyInRequestBody + " is required" }));
                    }
                }
                if (this.config.SEQUENCE_OF_STATES[this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":done"] !== undefined) {
                    return this.processMessage(__assign({}, parsedMessage, { state: this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state + ":done" }));
                }
            }
            this.produceMessage(this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.topic, this.config.SEQUENCE_OF_STATES[STATE][STATE_RESULT].nextState.state, parsedMessage.reqID, parsedMessage.message, this.config.QueConfig.consumerChannel);
        }
    };
    Common.prototype.produceMessage = function (topic, state, reqID, message, replyTopic, error) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var messageString = JSON.stringify({ reqID: reqID, message: message, replyTopic: replyTopic, state: state, error: error });
            switch (_this.QUE) {
                case "kafka":
                    if (_this.producer === undefined) {
                        return reject(new Error("producer not ready"));
                    }
                    _this.producer.send([
                        {
                            topic: topic,
                            messages: messageString
                        }
                    ], function (err, data) {
                        if (err) {
                            // console.log(err);
                            return reject(err);
                        }
                        else {
                            // console.log(`${reqID}: current state: ${state}`);
                            console.log('\x1b[33m%s\x1b[0m', "message sent to topic " + topic + ", state: " + state);
                            return resolve();
                        }
                    });
                    break;
                case "rabitMQ":
                    if (_this.rabbitMqChannel === undefined) {
                        return reject(new Error("channel not ready"));
                    }
                    _this.rabbitMqChannel.sendToQueue(topic, Buffer.from(messageString), {
                        persistent: true
                    });
                    console.log('\x1b[33m%s\x1b[0m', "message sent to topic " + topic + ", state: " + state);
                    return resolve();
                    break;
                default:
                    break;
            }
        });
    };
    return Common;
}());
var RabitMq = /** @class */ (function (_super) {
    __extends(RabitMq, _super);
    function RabitMq(configi, startConsuming) {
        var _this = _super.call(this, __assign({ QueConfig: { url: "amqp://localhost" } }, configi), "rabitMQ") || this;
        _this.startConsuming = startConsuming;
        return _this;
    }
    // init method to inilize async connection
    RabitMq.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var connection, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, amqp.connect(this.config.QueConfig.url)];
                    case 1:
                        connection = _b.sent();
                        _a = this;
                        return [4 /*yield*/, connection.createChannel()];
                    case 2:
                        _a.rabbitMqChannel = _b.sent();
                        this.rabbitMqChannel.assertQueue(this.config.QueConfig.consumerChannel, {
                            durable: true
                        });
                        this.startConsumingMessagesRabitMq();
                        return [2 /*return*/];
                }
            });
        });
    };
    RabitMq.prototype.startConsumingMessagesRabitMq = function () {
        var _this = this;
        //  todo: type for message from rabitMQ
        this.rabbitMqChannel.consume(this.config.QueConfig.consumerChannel, function (msg) {
            var parsedMessage = JSON.parse(msg.content.toString());
            console.log('\x1b[32m', "message received, state: " + parsedMessage.state);
            if (!_this.startConsuming) {
                _this.consumerEventEmitter.emit('message', parsedMessage);
            }
            else {
                _this.processMessage(parsedMessage);
            }
        }, {
            noAck: true
        });
    };
    return RabitMq;
}(Common));
exports.RabitMq = RabitMq;
var Kafka = /** @class */ (function (_super) {
    __extends(Kafka, _super);
    function Kafka(configi, startConsuming) {
        var _this = _super.call(this, __assign({ QueConfig: { url: "localhost:2181" } }, configi), "kafka") || this;
        _this.startConsuming = startConsuming;
        return _this;
    }
    // init method to inilize async connection
    Kafka.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var topicsToCreate, client, CONSUMER, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        topicsToCreate = [
                            {
                                topic: this.config.QueConfig.consumerChannel,
                                partitions: 1,
                                replicationFactor: 1
                            }
                        ];
                        return [4 /*yield*/, this.connectClientAndCreateTopics(this.config.QueConfig.url, topicsToCreate)];
                    case 1:
                        client = _b.sent();
                        return [4 /*yield*/, this.connectConsumer(client, this.config.QueConfig.consumerChannel)];
                    case 2:
                        CONSUMER = _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.connectProducer(client)];
                    case 3:
                        _a.producer = _b.sent();
                        this.startConsumingMessages(CONSUMER, this.startConsuming);
                        return [2 /*return*/];
                }
            });
        });
    };
    Kafka.prototype.connectClientAndCreateTopics = function (url, topicsToCreate) {
        return new Promise(function (resolve, reject) {
            var client = new kafka.KafkaClient(url);
            client.on("ready", function () {
                client.createTopics(topicsToCreate, function (error, result) {
                    if (error) {
                        console.log("error creating topics");
                        return reject(error);
                    }
                    return resolve(client);
                });
            });
            client.on("error", function (error) {
                console.log("client on error");
                return reject(error);
            });
        });
    };
    Kafka.prototype.connectConsumer = function (client, topic) {
        return new Promise(function (resolve, reject) {
            var Consumer = kafka.Consumer;
            var consumer = new Consumer(client, [{ topic: topic }], 
            // [{ topic: kafkaConfig.consumerChannel }],
            {
                autoCommit: true,
                fetchMaxWaitMs: 1000,
                fetchMaxBytes: 1024 * 1024,
                encoding: 'utf8',
                fromOffset: false
            });
            return resolve(consumer);
        });
    };
    Kafka.prototype.connectProducer = function (client) {
        return new Promise(function (resolve, reject) {
            var Producer = kafka.Producer;
            var producer = new Producer(client, { partitionerType: 2 });
            return resolve(producer);
        });
    };
    Kafka.prototype.startConsumingMessages = function (consumer, orchastrator) {
        var _this = this;
        consumer.on('message', function (message) { return __awaiter(_this, void 0, void 0, function () {
            var parsedMessage;
            return __generator(this, function (_a) {
                parsedMessage = JSON.parse(message.value);
                // console.log(parsedMessage.message);
                console.log('\x1b[32m', "message received, state: " + parsedMessage.state);
                // validation of state
                if (!orchastrator) {
                    this.consumerEventEmitter.emit('message', parsedMessage);
                }
                else {
                    this.processMessage(parsedMessage);
                }
                return [2 /*return*/];
            });
        }); });
    };
    return Kafka;
}(Common));
exports.Kafka = Kafka;
function rabitMqObject(configi, startConsuming) {
    return __awaiter(this, void 0, void 0, function () {
        var rabitMqObject;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rabitMqObject = new RabitMq(configi, startConsuming);
                    return [4 /*yield*/, rabitMqObject.init()];
                case 1:
                    _a.sent();
                    return [2 /*return*/, rabitMqObject];
            }
        });
    });
}
exports.rabitMqObject = rabitMqObject;
function kafkaObject(configi, startConsuming) {
    return __awaiter(this, void 0, void 0, function () {
        var kafkaObject;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    kafkaObject = new Kafka(configi, startConsuming);
                    return [4 /*yield*/, kafkaObject.init()];
                case 1:
                    _a.sent();
                    return [2 /*return*/, kafkaObject];
            }
        });
    });
}
exports.kafkaObject = kafkaObject;
