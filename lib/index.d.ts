export declare type STATES_T = 'create-cms' | 'create-cms-rollback' | 'create-url' | 'create-url-rollback' | 'create-tag' | 'create-tag-rollback' | 'put-wms-product' | 'put-wms-product-rollback' | 'patch-asset-manager' | 'patch-asset-manager-rollback' | 'create-cms:done' | 'create-cms:failed' | 'create-cms-rollback:done' | 'create-cms-rollback:failed' | 'create-url:done' | 'create-url:failed' | 'create-url-rollback:done' | 'create-url-rollback:failed' | 'create-tag:done' | 'create-tag:failed' | 'create-tag-rollback:done' | 'create-tag-rollback:failed' | 'put-wms-product:done' | 'put-wms-product:failed' | 'put-wms-product-rollback:done' | 'put-wms-product-rollback:failed' | 'patch-asset-manager:done' | 'patch-asset-manager:failed' | 'patch-asset-manager-rollback:done' | 'patch-asset-manager-rollback:failed';
export declare type STATE_RESULT_T = 'done' | 'failed';
export declare type settings = {
    [K in STATE_RESULT_T]?: {
        nextState?: {
            topic: string;
            state: STATES_T;
            keyInRequestBody: wrapperBodyKeys;
            mandatoryKey?: boolean;
        };
        rollback: boolean;
        success: boolean;
    };
};
export declare type SEQUENCE_OF_STATES_T = {
    [K in STATES_T]?: settings;
};
export interface configT {
    QueConfig: {
        url?: string;
        consumerChannel: string;
    };
    SEQUENCE_OF_STATES: SEQUENCE_OF_STATES_T;
}
export declare type wrapperBodyKeys = 'cms' | 'cmsRollback' | 'product' | 'productRollback' | 'productMeta' | 'productMetaRollback' | 'urlManager' | 'urlManagerRollback' | 'tag' | 'tagRollback' | 'wms' | 'wmsRollback' | 'assetManager' | 'assetManagerRollback' | 'lookbook' | 'lookbookRollback' | 'lookbookCategory' | 'lookbookCategoryRollback' | 'collection' | 'collectionRollback' | 'pages' | 'pagesRollback' | 'pagesCategory' | 'pagesCategoryRollback';
export declare type wrapperBodyWithoutCommonProperties = {
    [k in wrapperBodyKeys]?: any;
};
export declare type commonProperties = {
    commonProperties?: {
        vendorCode: string;
        source: string;
        sourceId: string;
    };
};
export declare type wrapperBody = wrapperBodyWithoutCommonProperties & commonProperties;
export interface StringifiedMessageI {
    reqID: string;
    message: wrapperBody;
    state: STATES_T;
    error: string;
    replyTopic: string;
}
declare type QUE_TYPE = 'kafka' | 'rabitMQ';
declare class Common {
    config: configT;
    QUE: QUE_TYPE;
    consumerEventEmitter: any;
    rabbitMqChannel: any;
    producer: any;
    constructor(config: configT, QUE: QUE_TYPE);
    processMessage(parsedMessage: StringifiedMessageI): any;
    produceMessage(topic: string, state: STATES_T, reqID: string, message: any, replyTopic: string, error?: string): Promise<{}>;
}
export declare class RabitMq extends Common {
    startConsuming?: boolean | undefined;
    constructor(configi: configT, startConsuming?: boolean | undefined);
    init(): Promise<void>;
    startConsumingMessagesRabitMq(): void;
}
export declare class Kafka extends Common {
    startConsuming?: boolean | undefined;
    constructor(configi: configT, startConsuming?: boolean | undefined);
    init(): Promise<void>;
    connectClientAndCreateTopics(url: string, topicsToCreate: any): Promise<any>;
    connectConsumer(client: any, topic: string): Promise<any>;
    connectProducer(client: any): Promise<any>;
    startConsumingMessages(consumer: any, orchastrator?: boolean): void;
}
export declare function rabitMqObject(configi: configT, startConsuming?: boolean): Promise<RabitMq>;
export declare function kafkaObject(configi: configT, startConsuming?: boolean): Promise<Kafka>;
export {};
